package com.example.ixigotest;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import AppConstants.AppConstants;

public class FlightSearchActivity extends AppCompatActivity {


    private EditText sourcePlace;
    private EditText destinationPlace;
    private Button searchButton;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_flight_search);
        sourcePlace = findViewById(R.id.source_place);
        destinationPlace = findViewById(R.id.destination_place);
        searchButton = findViewById(R.id.search_button);

        searchButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(TextUtils.isEmpty(sourcePlace.getText().toString()) || TextUtils.isEmpty(destinationPlace.getText().toString())){
                    Toast.makeText(FlightSearchActivity.this,getResources().getString(R.string.source_destination_empty_error),Toast.LENGTH_SHORT).show();
                } else {
                    Intent intent = new Intent(FlightSearchActivity.this, FlightListActivity.class);
                    intent.putExtra(AppConstants.SOURCE,sourcePlace.getText().toString());
                    intent.putExtra(AppConstants.DESTINATION,destinationPlace.getText().toString());
                    startActivity(intent);
                }

            }
        });
    }
}
