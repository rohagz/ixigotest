package com.example.ixigotest;

import android.arch.lifecycle.ViewModelProvider;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.widget.TextView;

import AppConstants.AppConstants;
import model.FlightDataModel;
import network.RetrofitModule;
import repo.FlightModelRepo;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import viewmodel.FlightListViewModel;

public class FlightListActivity extends AppCompatActivity {


    private FlightListViewModel viewModel;
    private TextView flightData;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_flight_list);
        flightData = findViewById(R.id.flightData);
        startLoadingFlights();
    }

    public void startLoadingFlights(){
        Intent intent = getIntent();
        if (intent!=null && intent.getExtras()!=null){
            String source = intent.getExtras().getString(AppConstants.SOURCE);
            String destination = intent.getExtras().getString(AppConstants.DESTINATION);
            Call<FlightDataModel> call = RetrofitModule.getNetworkService().getFlightDetails(AppConstants.FLIGHT_API_URL);
            call.enqueue(new Callback<FlightDataModel>() {
                @Override
                public void onResponse(Call<FlightDataModel> call, Response<FlightDataModel> response) {
                    String text = "";
                    if(response.isSuccessful() && response.body()!=null){
                        for(FlightDataModel.Flight flight:response.body().getFlights()){
                            text = text + flight.toString() + "\n";
                        }
                        flightData.setText(text);
                    }
                }

                @Override
                public void onFailure(Call<FlightDataModel> call, Throwable t) {
                    Log.d("","");
                    flightData.setText(t.toString());
                }
            });
        }
    }
    private void observer(){

    }
}
