package network;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RetrofitModule {

    private static Retrofit retrofit;
    private static NetworkService networkService;
    public static Retrofit getRetofitInstance(){
        if(retrofit == null){
            synchronized (RetrofitModule.class){
                retrofit = new Retrofit.Builder()
                        .baseUrl("https://www.mocky.io/")
                        .addConverterFactory(GsonConverterFactory.create())
                        .build();

            }
        }
        return retrofit;
    }

    public static NetworkService getNetworkService(){
        if(networkService == null){
            networkService =  getRetofitInstance().create(NetworkService.class);
        }
        return networkService;

    }
}
