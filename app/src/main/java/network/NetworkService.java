package network;

import android.arch.lifecycle.LiveData;

import model.FlightDataModel;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.GET;
import retrofit2.http.Url;

public interface NetworkService {
    @GET
    Call<FlightDataModel> getFlightDetails(@Url String url);
}
