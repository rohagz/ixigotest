package AppConstants;

public class AppConstants {
    public static final String SOURCE = "source";
    public static final String DESTINATION = "destination";
    public static final String FLIGHT_API_URL = "https://www.mocky.io/v2/5979c6731100001e039edcb3/";
}
