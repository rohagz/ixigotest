package repo;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.util.Log;

import AppConstants.AppConstants;
import model.FlightDataModel;
import network.NetworkService;
import network.RetrofitModule;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FlightModelRepo {

    private static FlightModelRepo repo;

    private FlightModelRepo(){

    }
    public static FlightModelRepo getInstance(){
        if(repo == null){
            synchronized (FlightModelRepo.class){
                repo = new FlightModelRepo();
            }
        }
        return repo;
    }



    public LiveData<FlightDataModel> getFlightDetails(String source,String dest) { // source,destination to be used in future

        MutableLiveData<FlightDataModel> liveData = new MutableLiveData<>();
        Call<FlightDataModel> call = RetrofitModule.getNetworkService().getFlightDetails(AppConstants.FLIGHT_API_URL);
        call.enqueue(new Callback<FlightDataModel>() {
            @Override
            public void onResponse(Call<FlightDataModel> call, Response<FlightDataModel> response) {
                if(response.isSuccessful() && response.body()!=null){

                }
            }

            @Override
            public void onFailure(Call<FlightDataModel> call, Throwable t) {
                Log.d("","");
            }
        });
        return liveData;
    }
}
