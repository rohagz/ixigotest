package model;

import android.support.annotation.NonNull;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

import repo.FlightModelRepo;

public class FlightDataModel {

    public FlightDataModel(){

    }


    public static class Appendix {

    }
    private List<Flight> flights;


    public List<Flight> getFlights() {
        return flights;
    }

    public void setFlights(List<Flight> flights) {
        this.flights = flights;
    }

    public static class Flight {


        private String originCode;
        private String destinationCode;
        private double departureTime;
        private double arrivalTime;
        private String airlineCode;
        @SerializedName("class")
        private String clasS;


        public String getAirlineCode() {
            return airlineCode;
        }

        public String getClasS() {
            return clasS;
        }

        public void setAirlineCode(String airlineCode) {
            this.airlineCode = airlineCode;
        }

        public void setClasS(String clasS) {
            this.clasS = clasS;
        }

        public double getArrivalTime() {
            return arrivalTime;
        }

        public double getDepartureTime() {
            return departureTime;
        }

        public String getDestinationCode() {
            return destinationCode;
        }

        public String getOriginCode() {
            return originCode;
        }

        public void setArrivalTime(double arrivalTime) {
            this.arrivalTime = arrivalTime;
        }

        public void setDepartureTime(double departureTime) {
            this.departureTime = departureTime;
        }

        public void setDestinationCode(String destinationCode) {
            this.destinationCode = destinationCode;
        }

        public void setOriginCode(String originCode) {
            this.originCode = originCode;
        }

        public Flight(){

        }

        @NonNull
        @Override
        public String toString() {
            return "origin:" + originCode + ":" + departureTime + " to " + "destination:" + destinationCode + ":" + arrivalTime;
        }

        public static class Fare {
            private long providerId;
            private long fare;

            public Fare(){

            }

            public long getFare() {
                return fare;
            }

            public long getProviderId() {
                return providerId;
            }

            public void setFare(long fare) {
                this.fare = fare;
            }

            public void setProviderId(long providerId) {
                this.providerId = providerId;
            }
        }

    }
}
